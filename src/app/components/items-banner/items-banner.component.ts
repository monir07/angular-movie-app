import {Component, Input} from '@angular/core';
import {Movie} from "../../models/movie";
import {Tv} from "../../models/tvshows";

@Component({
   selector: 'app-items-banner',
   templateUrl: './items-banner.component.html',
   styleUrls: ['./items-banner.component.scss']
})
export class ItemsBannerComponent {
   @Input() items: Movie[] = [];
   @Input() tvItems: Tv[] = [];
   @Input() title: string = '';


}
