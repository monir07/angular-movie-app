import {Component, Input} from '@angular/core';
import {Movie} from "../../models/movie";
import {IMAGES_SIZES} from "../../constants/images-sizes";
import {Tv} from "../../models/tvshows";

@Component({
   selector: 'app-item',
   templateUrl: './item.component.html',
   styleUrls: ['./item.component.scss']
})
export class ItemComponent {
   @Input() itemData: Movie | null = null;
   @Input() tvItemData: Tv | null = null;

   imagesSizes = IMAGES_SIZES

}
