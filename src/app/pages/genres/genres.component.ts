import {Component, OnInit} from '@angular/core';
import {Genre} from "../../models/genre";
import {MoviesService} from "../../services/movies.service";
import {TVGenre} from "../../models/tvgenre";
import {TvshowsService} from "../../services/tvshows.service";

@Component({
   selector: 'app-genres',
   templateUrl: './genres.component.html',
   styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {
   genres: Genre[] = [];
   tvGenres: TVGenre[] = [];

   constructor(private moviesService: MoviesService,
               private tvshowsService:TvshowsService) {
   }

   ngOnInit(): void {
      this.moviesService.getMoviesGenres().subscribe((genresData) => {
         this.genres = genresData;
      });

      this.tvshowsService.getTVShowsGenres().subscribe((genresData) => {
         this.tvGenres = genresData;
      });

   }

}
