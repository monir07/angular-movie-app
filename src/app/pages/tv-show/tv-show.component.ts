import {Component, OnDestroy, OnInit} from '@angular/core';
import {Tv} from "../../models/tvshows";
import {MovieCredits, MovieImages, MovieVideo} from "../../models/movie";
import {IMAGES_SIZES} from "../../constants/images-sizes";
import {ActivatedRoute} from "@angular/router";
import {TvshowsService} from "../../services/tvshows.service";

@Component({
   selector: 'app-tv-show',
   templateUrl: './tv-show.component.html',
   styleUrls: ['./tv-show.component.scss']
})
export class TvShowComponent implements OnInit, OnDestroy {
   tv: Tv | null = null;
   tvVideos: MovieVideo[] = [];
   tvImages: MovieImages | null = null;
   tvCredits: MovieCredits | null = null;
   similarTVShows: Tv[] = [];
   imagesSizes = IMAGES_SIZES;

   constructor(private route: ActivatedRoute,
               private tvshowsService: TvshowsService) {
   }

   ngOnInit(): void {
      this.route.params.pipe().subscribe(({id}) => {
         this.getTVShowDetails(id);
         this.getTVShowVideos(id);
         this.getTVShowImages(id);
         this.getTVShowCredits(id);
         this.getTVShowSimilar(id);
      })
   }

   ngOnDestroy() {
      console.log('component destroyed');
   }

   getTVShowDetails(id: string) {
      this.tvshowsService.getTVShowDetails(id).subscribe((showData) => {
         this.tv = showData
      })
   }

   getTVShowVideos(id: string) {
      this.tvshowsService.getTVShowVideo(id).subscribe((showVideoData) => {
         this.tvVideos = showVideoData.results
      })
   }

   getTVShowImages(id: string) {
      this.tvshowsService.getTVShowImages(id).subscribe((showImagesData) => {
         this.tvImages = showImagesData
      })
   }

   getTVShowCredits(id: string) {
      this.tvshowsService.getTVShowCredits(id).subscribe((showCreditsData) => {
         this.tvCredits = showCreditsData;
      });
   }

   getTVShowSimilar(id: string) {
      this.tvshowsService.getTVShowSimilar(id).subscribe((showSimilarData) => {
         this.similarTVShows = showSimilarData;
         console.log(this.similarTVShows)
      });
   }


}
