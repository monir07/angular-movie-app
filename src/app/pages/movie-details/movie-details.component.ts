import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MoviesService} from "../../services/movies.service";
import {Movie, MovieCredits, MovieImages, MovieVideo} from "../../models/movie";
import {IMAGES_SIZES} from "../../constants/images-sizes";

@Component({
   selector: 'app-movie-details',
   templateUrl: './movie-details.component.html',
   styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {
   movie: Movie | null = null;
   movieVideos: MovieVideo[] = [];
   movieImages: MovieImages | null = null;
   movieCredits: MovieCredits | null = null;
   similarMovies: Movie[] = [];
   imagesSizes = IMAGES_SIZES;

   constructor(private route: ActivatedRoute,
               private moviesService: MoviesService) {
   }

   ngOnInit(): void {
      this.route.params.pipe().subscribe(({id}) => {
         this.getMovieDetails(id);
         this.getMovieVideos(id);
         this.getMovieImages(id);
         this.getMovieCredits(id);
         this.getMovieSimilar(id);
      })
   }

   ngOnDestroy() {
      console.log('component destroyed');
   }

   getMovieDetails(id: string) {
      this.moviesService.getMovieDetails(id).subscribe((movieData) => {
         this.movie = movieData
      })
   }

   getMovieVideos(id: string) {
      this.moviesService.getMovieVideo(id).subscribe((movieVideoData) => {
         this.movieVideos = movieVideoData.results
      })
   }

   getMovieImages(id: string) {
      this.moviesService.getMovieImages(id).subscribe((movieImagesData) => {
         this.movieImages = movieImagesData
      })
   }

   getMovieCredits(id: string) {
      this.moviesService.getMovieCredits(id).subscribe((movieCreditsData) => {
         this.movieCredits = movieCreditsData;
      });
   }

   getMovieSimilar(id: string) {
      this.moviesService.getMovieSimilar(id).subscribe((movieSimilarData) => {
         this.similarMovies = movieSimilarData;
      });
   }

}
