import {Component, OnInit} from '@angular/core';
import {MoviesService} from "../../services/movies.service";
import {Movie} from "../../models/movie";
import {Tv} from "../../models/tvshows";
import {TvshowsService} from "../../services/tvshows.service";

@Component({
   selector: 'app-home',
   templateUrl: './home.component.html',
   styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
   popularMovies: Movie[] = [];
   upcomingMovies: Movie[] = [];
   topRatedMovies: Movie[] = [];
   nowPlayingMovies: Movie[] = [];
   topRatedTVShows: Tv[] = [];
   onTheAirTvShows:Tv[]=[];

   constructor(private moviesService: MoviesService,
               private tvshowsService:TvshowsService) {
   }

   ngOnInit(): void {
      this.moviesService.getMovies('popular', 10).subscribe((movies) => {
         this.popularMovies = movies;
      });
      this.moviesService.getMovies('upcoming', 12).subscribe((movies) => {
         this.upcomingMovies = movies;
      });
      this.moviesService.getMovies('top_rated', 12).subscribe((movies) => {
         this.topRatedMovies = movies;
      });
      this.moviesService.getMovies('now_playing', 12).subscribe((movies) => {
         this.nowPlayingMovies = movies;
      });
      this.tvshowsService.getTvs('top_rated', 12).subscribe((tvShows) => {
         this.topRatedTVShows = tvShows;
      });
      this.tvshowsService.getTvs('on_the_air', 12).subscribe((tvShows) => {
         this.onTheAirTvShows = tvShows;
      });
   }

}
