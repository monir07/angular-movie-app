import {Component, OnInit} from '@angular/core';
import {Tv} from "../../models/tvshows";
import {TvshowsService} from "../../services/tvshows.service";
import {ActivatedRoute} from "@angular/router";
import {take} from "rxjs";

@Component({
   selector: 'app-tv-shows',
   templateUrl: './tv-shows.component.html',
   styleUrls: ['./tv-shows.component.scss']
})
export class TvShowsComponent implements OnInit {
   tvShows: Tv[] = [];
   genreId: string | null = null;
   searchValue: string | null = null;

   type: string = 'popular';
   page: number = 1;

   constructor(private tvshowsService: TvshowsService,
               private route: ActivatedRoute) {
   }

   ngOnInit(): void {
      this.route.params.pipe(take(1)).subscribe(({genreId}) => {
         if (genreId) {
            this.genreId = genreId;
            this.getTVShowsByGenre(genreId, 1);
         } else {
            this.getPagedTVShows(this.type, 1);
         }
      })
   }

   getTVShowsByGenre(genreId: string, page: number) {
      this.tvshowsService.getTVShowsByGenre(genreId, page).subscribe((shows) => {
         this.tvShows = shows.results;
      });
   }

   getPagedTVShows(type: string, page: number,  searchKeyword?: string): void {
      this.tvshowsService.searchTVShows(type, page, searchKeyword).subscribe(shows => {
         this.tvShows = shows.results;
      })
   }

   paginate(event: any) {
      const pageNumber = event.page + 1;
      if (this.genreId) {
         this.getTVShowsByGenre(this.genreId, pageNumber);
      } else {
         if (this.searchValue) {
            this.getPagedTVShows(this.type,pageNumber, this.searchValue);
         } else {
            this.getPagedTVShows(this.type, pageNumber);
         }
      }
   }

   searchChanges() {
      if (this.searchValue) {
         this.getPagedTVShows(this.type,1, this.searchValue)
      }
   }

}
