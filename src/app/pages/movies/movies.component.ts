import {Component, OnInit} from '@angular/core';
import {MoviesService} from "../../services/movies.service";
import {Movie} from "../../models/movie";
import {ActivatedRoute} from "@angular/router";
import {take} from "rxjs";

@Component({
   selector: 'app-movies',
   templateUrl: './movies.component.html',
   styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
   movies: Movie[] = [];
   genreId: string | null = null;
   searchValue: string | null = null;

   type: string = 'popular';
   page: number = 1;

   constructor(private moviesService: MoviesService,
               private route: ActivatedRoute) {
   }

   ngOnInit(): void {
      this.route.params.pipe(take(1)).subscribe(({genreId}) => {
         if (genreId) {
            this.genreId = genreId;
            this.getMoviesByGenre(genreId, 1);
         } else {
            this.getPagedMovies(this.type, 1);
         }
      })
   }

   getMoviesByGenre(genreId: string, page: number) {
      this.moviesService.getMoviesByGenre(genreId, page).subscribe((movies) => {
         this.movies = movies.results;
      });
   }

   getPagedMovies(type: string, page: number,  searchKeyword?: string): void {
      this.moviesService.searchMovies(type, page, searchKeyword).subscribe(movies => {
         this.movies = movies.results;
      })
   }

   paginate(event: any) {
      const pageNumber = event.page + 1;
      if (this.genreId) {
         this.getMoviesByGenre(this.genreId, pageNumber);
      } else {
         if (this.searchValue) {
            this.getPagedMovies(this.type,pageNumber, this.searchValue);
         } else {
            this.getPagedMovies(this.type, pageNumber);
         }
      }
   }

   searchChanges() {
      if (this.searchValue) {
         this.getPagedMovies(this.type,1, this.searchValue)
      }
   }

}
