import {Movie} from './movie';

export interface Tv extends Movie {
   name: string;
   first_air_date: string;
   number_of_episodes:number;
   number_of_seasons: number;
   last_air_date:string;
}

export interface TvDatato {
   page: number;
   results: Tv[];
   total_results: number;
   total_pages: number;
}
