export interface Movie {
   adult: boolean;
   backdrop_path: string;
   genre_ids: number[];
   id: number;
   original_language: string;
   original_title: string;
   overview: string;
   popularity: number;
   poster_path: string;
   release_date: string;
   title: string;
   video: boolean;
   vote_average: number;
   vote_count: number;
   budget: number;
   revenue: number;
   runtime: number;
   status: string;
   genres: Genre[];
}

export interface MovieDataTo {
   page: number;
   results: Movie[];
   total_results: number;
   total_pages: number;
}

export interface Genre {
   id: number;
   name: string;
}

export interface MovieVideoDateTo {
   id: number;
   results: MovieVideo[];
}

export interface MovieVideo {
   id: number;
   key: string;
   site: string;
   size: number;
   name: string | null;
   type: string;
   official: boolean;
   published_at: string;
   iso_639_1: string | null;
   iso_3166_1: string | null;
}

export interface MovieImages {
   backdrops: {
      file_path: string;
   }[];
}

export interface MovieCredits {
   cast: {
      name: string;
      profile_path: string;
   }[];
}
