export interface TVGenresDto {
   genres: TVGenre[];
}

export interface TVGenre {
   name: string;
   id: string;
}
