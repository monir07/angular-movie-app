import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Movie, MovieCredits, MovieDataTo, MovieImages, MovieVideoDateTo} from "../models/movie";
import {of, switchMap} from "rxjs";
import {GenresDto} from "../models/genre";
import {TvDatato} from "../models/tvshows";

@Injectable({
   providedIn: 'root'
})
export class MoviesService {
   baseUrl: string = 'https://api.themoviedb.org/3';
   apiKey: string = '02e7335ab2ba61a5d39d7ce8e613b564';

   constructor(private http: HttpClient) {
   }

   // 'movie/upcoming?api_key=&language=en-US&page=1'
   getMovies(type: string = 'upcoming', count: number = 12) {
      return this.http.get<MovieDataTo>(`${this.baseUrl}/movie/${type}?api_key=${this.apiKey}&language=en-US`)
         .pipe(switchMap((res) => {
               return of(res.results.slice(0, count));
            })
         );
   }

   searchMovies(type: string = 'upcoming', page: number, searchValue?: string) {
      const uri = searchValue ? '/search/movie' : '/movie/' + type;
      return this.http.get<MovieDataTo>(`${this.baseUrl}${uri}?api_key=${this.apiKey}&query=${searchValue}&language=en-US&page=${page}`)
         .pipe(switchMap((res) => {
               return of(res);
            })
         );
   }

   getMovieDetails(movie_id: string) {
      return this.http.get<Movie>(`${this.baseUrl}/movie/${movie_id}?api_key=${this.apiKey}&language=en-US`);
   }

   getMovieVideo(movie_id: string) {
      return this.http.get<MovieVideoDateTo>(`${this.baseUrl}/movie/${movie_id}/videos?api_key=${this.apiKey}&language=en-US`)
         .pipe(switchMap((res) => {
               return of(res);
            })
         );
   }

   getMovieImages(movie_id: string) {
      return this.http.get<MovieImages>(`${this.baseUrl}/movie/${movie_id}/images?api_key=${this.apiKey}&language=en-US`);
   }

   getMovieCredits(id: string) {
      return this.http.get<MovieCredits>(`${this.baseUrl}/movie/${id}/credits?api_key=${this.apiKey}&language=en-US`);
   }

   getMoviesGenres() {
      return this.http.get<GenresDto>(`${this.baseUrl}/genre/movie/list?api_key=${this.apiKey}&language=en-US`).pipe(
         switchMap((res) => {
            return of(res.genres);
         })
      );
   }

   getMoviesByGenre(genreId: string, pageNumber: number) {
      return this.http
         .get<MovieDataTo>(
            `${this.baseUrl}/discover/movie?api_key=${this.apiKey}&language=en-US&with_genres=${genreId}&page=${pageNumber}`
         )
         .pipe(
            switchMap((res) => {
               return of(res);
            })
         );
   }

   getMovieSimilar(movie_id: string) {
      return this.http
         .get<TvDatato>(`${this.baseUrl}/movie/${movie_id}/similar?api_key=${this.apiKey}`)
         .pipe(
            switchMap((res) => {
               return of(res.results.slice(0, 12));
            })
         );
   }

}
