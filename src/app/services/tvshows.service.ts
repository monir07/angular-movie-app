import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Tv, TvDatato} from "../models/tvshows";
import {of, switchMap} from "rxjs";
import {MovieCredits, MovieImages, MovieVideoDateTo} from "../models/movie";
import {TVGenresDto} from "../models/tvgenre";

@Injectable({
   providedIn: 'root'
})
export class TvshowsService {
   baseUrl: string = 'https://api.themoviedb.org/3';
   apiKey: string = '02e7335ab2ba61a5d39d7ce8e613b564';

   constructor(private http: HttpClient) {
   }

   getTvs(type: string = 'latest', count: number = 12) {
      return this.http.get<TvDatato>(`${this.baseUrl}/tv/${type}?api_key=${this.apiKey}&language=en-US`)
         .pipe(switchMap((res) => {
               return of(res.results.slice(0, count));
            })
         );
   }

   searchTVShows(type: string = 'latest', page: number, searchValue?: string) {
      const uri = searchValue ? '/search/tv' : '/tv/' + type;
      return this.http.get<TvDatato>(`${this.baseUrl}${uri}?api_key=${this.apiKey}&query=${searchValue}&language=en-US&page=${page}`)
         .pipe(switchMap((res) => {
               return of(res);
            })
         );
   }

   getTVShowDetails(tvShow_id: string) {
      return this.http.get<Tv>(`${this.baseUrl}/tv/${tvShow_id}?api_key=${this.apiKey}&language=en-US`);
   }

   getTVShowVideo(tvShow_id: string) {
      return this.http.get<MovieVideoDateTo>(`${this.baseUrl}/tv/${tvShow_id}/videos?api_key=${this.apiKey}&language=en-US`)
         .pipe(switchMap((res) => {
               return of(res);
            })
         );
   }

   getTVShowImages(tvShow_id: string) {
      return this.http.get<MovieImages>(`${this.baseUrl}/tv/${tvShow_id}/images?api_key=${this.apiKey}&language=en-US`);
   }

   getTVShowCredits(tvShow_id: string) {
      return this.http.get<MovieCredits>(`${this.baseUrl}/tv/${tvShow_id}/credits?api_key=${this.apiKey}&language=en-US`);
   }

   getTVShowsGenres() {
      return this.http.get<TVGenresDto>(`${this.baseUrl}/genre/tv/list?api_key=${this.apiKey}&language=en-US`).pipe(
         switchMap((res) => {
            return of(res.genres);
         })
      );
   }

   getTVShowsByGenre(genreId: string, pageNumber: number) {
      return this.http
         .get<TvDatato>(
            `${this.baseUrl}/discover/tv?api_key=${this.apiKey}&language=en-US&with_genres=${genreId}&page=${pageNumber}`
         )
         .pipe(
            switchMap((res) => {
               return of(res);
            })
         );
   }

   getTVShowSimilar(tvShow_id: string) {
      return this.http
         .get<TvDatato>(`${this.baseUrl}/tv/${tvShow_id}/similar?api_key=${this.apiKey}`)
         .pipe(
            switchMap((res) => {
               return of(res.results.slice(0, 12));
            })
         );
   }

}
