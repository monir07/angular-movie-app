import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {MoviesComponent} from "./pages/movies/movies.component";
import {MovieDetailsComponent} from "./pages/movie-details/movie-details.component";
import {GenresComponent} from "./pages/genres/genres.component";
import {TvShowsComponent} from "./pages/tv-shows/tv-shows.component";
import {TvShowComponent} from "./pages/tv-show/tv-show.component";

const routes: Routes = [
   {
      path: '',
      component: HomeComponent
   },
   {
      path: 'movies',
      component: MoviesComponent
   },
   {
      path: 'movie-details/:id',
      component: MovieDetailsComponent
   },
   {
      path: 'genres',
      component: GenresComponent
   },
   {
      path: 'movies/genres/:genreId',
      component: MoviesComponent
   },
   {
      path: 'tvshows',
      component: TvShowsComponent
   },
   {
      path: 'tv-shows-details/:id',
      component: TvShowComponent
   },
   {
      path: 'tv-shows/genres/:genreId',
      component: TvShowsComponent
   },
   {
      path: '**',
      redirectTo: ''
   }
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule]
})
export class AppRoutingModule {
}
